# README #

This program is designed to allow a user to login, register up to four golf players,
then keep track of the scores of each player.

### How do I get set up? ###

Clone the repository, then run the program through the NetBeans IDE while hosting a TomCat server.


### Who do I talk to? ###

* Creator: Blake Buchanan
* Peer Reviewer: Patch Smith