package info.hccis.tournament.web;

import info.hccis.tournament.entity.Tournament;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import info.hccis.tournament.data.springdatajpa.TournamentRepository;

@Controller
public class TournamentController {

    private final TournamentRepository tournamentRepository;

    @Autowired
    public TournamentController(TournamentRepository tournamentRepository) {
        this.tournamentRepository = tournamentRepository;
    }

    @RequestMapping("/tournament/add")
    public String tournamentAdd(Model model) {
        System.out.println("BJM-in controller for /tournament/add");
        Tournament temp = new Tournament();
        model.addAttribute("tournament", temp);
        
        
        //TESTING adding strings to collection which could be used for custom errors.
        //http://stackoverflow.com/questions/33106391/how-to-check-if-list-is-empty-using-thymeleaf
        ArrayList<String> businessErrors = new ArrayList();
        businessErrors.add("Test1");
        businessErrors.add("Test2");
        model.addAttribute("businessErrors", businessErrors);
        
        
        
        return "tournament/add";
    }

    @RequestMapping("/tournament/edit")
    public String tournamentAddSubmit(Model model, HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("BJM-/tournament/edit -->id=" + id);
        
        //get the camper from the repository based on the id
        Tournament temp = tournamentRepository.findOne(id);
        //send the user back to the add page after putting the camper object in the model
        model.addAttribute("tournament", temp);
        return "tournament/add";
        
       
    }

    @RequestMapping("/tournament/addSubmit")
    public String tournamentAddSubmit(Model model, @Valid @ModelAttribute("tournament") Tournament tournament, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Error in validation of tournament.");
            
                    //TESTING adding strings to collection which could be used for custom errors.
            //http://stackoverflow.com/questions/33106391/how-to-check-if-list-is-empty-using-thymeleaf
        ArrayList<String> businessErrors = new ArrayList();
        businessErrors.add("Test1");
        businessErrors.add("Test2");
        model.addAttribute("businessErrors", businessErrors);
            
            return "/tournament/add";
        }
        
        
        try {
            //System.out.println("camper name =" + camper.getFirstName());

               // camper.setId(1);
            tournamentRepository.save(tournament);
            
//            CamperDAO.update(camper);
            //send to the list of campers view.
            //ArrayList<Camper> campers = CamperDAO.selectAll();
            ArrayList<Tournament> tournaments = (ArrayList<Tournament>) tournamentRepository.findAll();
            model.addAttribute("theTournaments", tournaments);

            return "/tournament/list";
        } catch (Exception ex) {
            System.out.println("There was an error adding the tournament.");
            return "";
        }
    }
}
