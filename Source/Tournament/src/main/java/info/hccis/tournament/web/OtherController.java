package info.hccis.tournament.web;

import info.hccis.tournament.dao.CodeValueDAO;
import info.hccis.tournament.dao.UserAccessDAO;
import info.hccis.tournament.entity.Tournament;
import info.hccis.tournament.entity.CodeValue;
import info.hccis.tournament.entity.UserAccess;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import info.hccis.tournament.data.springdatajpa.TournamentRepository;

@Controller
public class OtherController {

        private final TournamentRepository camperRepository;

    @Autowired
    public OtherController(TournamentRepository camperRepository) {
        this.camperRepository = camperRepository;
    }
    
//    @RequestMapping("/")
//    public String showHome(Model model) {
//        System.out.println("in controller for /");
//
//        ArrayList<Camper> campers = CamperDAO.selectAll();
//        System.out.println("Number of campers = " + campers.size());
//        model.addAttribute("theCampers", campers);
//
//        return "camper/list";
//    }
    @RequestMapping("/")
    public String showHome(Model model) {
        UserAccess user = new UserAccess();
        user.setUsername("test");
        model.addAttribute("user", user);
        
        //TESTING passing this to the html page to test showing it in a message.
        model.addAttribute("testNumber","5");
        return "camper/welcome";
    }

    @RequestMapping("/authenticate")
    public String authenticate(Model model, @ModelAttribute("user") UserAccess user, HttpServletRequest request, HttpSession session) {

        /*
        
        See the @ModelAttribute refers to the object that is used on the form.  This is
        obtained here in the controller.  This is a common way that the controller gets access
        to the object and its attributes.  
        
         */
//Test to show what was passed in.  
        System.out.println("BJM in /authenticate, username passed in to controller is:" + user.getUsername());

        //Check for access of the user
        String accessLevel;
        if (user.getUsername() == null || user.getUsername().equals("test")) {
            accessLevel = "1";
        } else {
            accessLevel = UserAccessDAO.getUserTypeCode(user.getUsername(), user.getPassword());
        }
        if (accessLevel.equals("0")) {
            return "/camper/welcome";
        } else {

            //        Here we will store the user in the session.  It will then be available to 
            //        other future requests.  
            request.getSession().setAttribute("user", user);

            //Set an object in the model to be used on the next form.  
            ArrayList<Tournament> campers = (ArrayList<Tournament>) camperRepository.findAll();
            //ArrayList<Camper> campers = CamperDAO.selectAll();
            System.out.println("Number of campers = " + campers.size());
            model.addAttribute("theCampers", campers);



            //Setup the Camp Types
        ArrayList<CodeValue> campTypes = (ArrayList<CodeValue>) CodeValueDAO.getCodeValues("2");

        //Store this in the session
        session.setAttribute("CampTypes", campTypes);
            
            
            
            
            
            
            
/*            
        Specify the return.  This is the next view that will be presented to the user.
             */
            return "/camper/list";
        }
    }

    @RequestMapping("/newOne")
    public String showNewOne(Model model) {
        System.out.println("in controller for /newOne");
        return "camper/newOne";

        /*
        After this have to make sure that this html page exists. This would be 
        in the WEB-INF/thymeleaf/camper/
         */
    }

}
