package info.hccis.tournament.data.springdatajpa;


import info.hccis.tournament.entity.Tournament;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TournamentRepository extends CrudRepository<Tournament, Integer> {


}