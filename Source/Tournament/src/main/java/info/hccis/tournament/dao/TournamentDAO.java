package info.hccis.tournament.dao;

import info.hccis.tournament.entity.Tournament;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * This class will contain db functionality for working with Tournaments
 *
 * @author bjmaclean
 * @since 20160929
 */
public class TournamentDAO {

    private final static Logger LOGGER = Logger.getLogger(TournamentDAO.class.getName());

    public static Tournament select(int idIn) {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        Tournament theTournament = null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM tournament where id = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, idIn);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                int id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String dob = rs.getString("dob");
                Tournament temp = new Tournament();
                temp.setId(id);
                temp.setFirstName(firstName);
                temp.setLastName(lastName);
                temp.setDob(dob);

                theTournament = temp;
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return theTournament;
    }

    /**
     * Select all campers
     *
     * @author bjmaclean
     * @since 20160929
     * @return
     */
    public static ArrayList<Tournament> selectAll() {

        ArrayList<Tournament> tournaments = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM tournament order by id";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                int id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String dob = rs.getString("dob");
                Tournament temp = new Tournament();
                temp.setId(id);
                temp.setFirstName(firstName);
                temp.setLastName(lastName);
                temp.setDob(dob);
                tournaments.add(temp);

            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return tournaments;
    }

    public static ArrayList<Tournament> selectAllByDOB(String year) {

        ArrayList<Tournament> tournaments = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM tournament where dob like '" + year + "' order by id";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                int id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String dob = rs.getString("dob");
                Tournament temp = new Tournament();
                temp.setId(id);
                temp.setFirstName(firstName);
                temp.setLastName(lastName);
                temp.setDob(dob);
                tournaments.add(temp);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return tournaments;
    }

    /**
     * This method will insert.
     *
     * @return
     * @author BJ
     * @since 20140615
     */
    public static synchronized Tournament update(Tournament tournament) throws Exception {
//        System.out.println("inserting camper");
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;


        /*
         * Setup the sql to update or insert the row.
         */
        try {
            conn = ConnectionUtils.getConnection();

            //Check to see if camper exists.
            if (tournament.getId() == null ||tournament.getId() == 0) {

                sql = "SELECT max(id) from Camper";
                ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                int max = 0;
                while (rs.next()) {
                    max = rs.getInt(1) + 1;
                }

                tournament.setId(max);

                sql = "INSERT INTO `Camper`(`id`, `firstName`, `lastName`, `dob`) "
                        + "VALUES (?,?,?,?)";

                ps = conn.prepareStatement(sql);
                ps.setInt(1, tournament.getId());
                ps.setString(2, tournament.getFirstName());
                ps.setString(3, tournament.getLastName());
                ps.setString(4, tournament.getDob());

            } else {

                sql = "UPDATE `Tournament` SET `firstName`=?,`lastName`=?,`dob`=? WHERE id = ?";

                ps = conn.prepareStatement(sql);
                ps.setString(1, tournament.getFirstName());
                ps.setString(2, tournament.getLastName());
                ps.setString(3, tournament.getDob());
                ps.setInt(4, tournament.getId());

            }
            /*
             Note executeUpdate() for update vs executeQuery for read only!!
             */

            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } finally {
            DbUtils.close(ps, conn);
        }
        return tournament;

    }

}

//    /**
//     * This method will insert.
//     *
//     * @return
//     * @author BJ
//     * @since 20140615
//     */
//    public static void insertNotification(Notification notification) throws Exception {
//        System.out.println("inserting notification");
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//
//        /*
//         * Setup the sql to update or insert the row.
//         */
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "INSERT INTO `notification`(`notification_type_code`,  "
//                    + "`notification_detail`, `status_code`, `created_date_time`, "
//                    + "`created_user_id`, `updated_date_time`, `updated_user_id`) "
//                    + "VALUES (?, ?, 1, sysdate(), ?, sysdate(), ?)";
//
//            ps = conn.prepareStatement(sql);
//            ps.setInt(1, notification.getNotificationType());
//            ps.setString(2, notification.getNotificationDetail());
//            ps.setString(3, notification.getUserId());
//            ps.setString(4, notification.getUserId());
//
//            ps.executeUpdate();
//
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//            throw e;
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return;
//
//    }
//
//    /**
//     * Delete the specified member education (set to inactive)
//     * @param memberId
//     * @param memberEducationSequence 
//     */
//    public static void deleteNotification(int notificationId) throws Exception{
//        
//        System.out.println("deleting notification");
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//
//        /*
//         * Setup the sql to update or insert the row.
//         */
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "update notification set status_code = 0, updated_date_time = sysdate() "
//                + "where notification_id = ? ";
//
//            ps = conn.prepareStatement(sql);
//            ps.setInt(1, notificationId);
//
//            ps.executeUpdate();
//
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//            throw e;
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return;
//
//    }
//        
//
//    
//    public static ArrayList<Notification> getNotifications() {
//        ArrayList<Notification> notifications = new ArrayList();
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "SELECT * FROM notification WHERE status_code = 1 order by created_date_time desc";
//
//            ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                Notification newNotification = new Notification();
//                newNotification.setNotificationId(rs.getInt("notification_id"));
//                newNotification.setNotificationDetail(rs.getString("notification_detail"));
//                newNotification.setNotificationType(rs.getInt("notification_type_code"));
//                newNotification.setNotificationDate(rs.getString("created_date_time"));
//                newNotification.setUserId(rs.getString("created_user_id"));
//                notifications.add(newNotification);
//            }
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return notifications;
//    }

